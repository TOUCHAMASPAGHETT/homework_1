function distance(first, second){
	// TODO: implement the function
	
	if (!(Array.isArray(first) && Array.isArray(second)))
		throw new Error('InvalidType')
	
	if (first.length == 0 && second.length == 0)
		return 0
	

	// first we remove duplicates from both arrays to keep it simple
	// I learned in Maths class that n(A)+n(B)−2n(A∩B)=n(A△B), where n is the number of elements of a set
	// I figured the same rules apply to arrays, hopefully

	const uniqueFirst = first.filter((e, i) => first.indexOf(e) === i);
	const uniqueSecond = second.filter((e, i) => second.indexOf(e) === i);

	let intersection = uniqueFirst.filter((e) => uniqueSecond.indexOf(e) !== -1);
	return uniqueFirst.length + uniqueSecond.length - 2 * intersection.length;

}


module.exports.distance = distance